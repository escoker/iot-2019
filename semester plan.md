---
title: 'Week Based Lecture Plan for Embedded Systems'
subtitle: 'First Semester'
author: ['Ruslan Trifonov \<rutr@ucl.dk>']
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: 'Embedded Systems 2019'
---


# Week 35 adn Week 36 Topics

Iam away in Week 35 and 36, which means in later weeks we will have extra classes.



# Week 37 Topics

IoT systems over, view. Before building an IoT solution examing the enviroment.


## Week 37 Tasks

1. See an IoT case from a real project.
2. Find a course project with focus on IoT.
3. Generate documentation and study case.

## Week 37 Deliverables

1. Hand-in Project and Case


# Week 37 Topics

IoT wireless communication.


## Week 37 Tasks

1. Students will be presentaed various IoT communication technologies
2. Students will select the one that works best for them and their project.

## Week 37 Deliverables

1. Order IoT network nodes.
2. Students will start to design there solutions.


# Week 38 Topics

Work on IoT communication technology 

## Week 38 Tasks

1. The technology selected in the previews week will be presented in to more details.

## Week 38 Deliverables

1. Students will start to work on their solutions.


# Week 38 Topics

IoT and security.

## Week 38 Tasks

1. Investigate ways to protect your network and IoT devices.
2. Example of encryptions and security.

## Week 38 Deliverables
1. Work on IoT project


# Week 39 Topics

Work on IoT project

## Week 39 Tasks

1. Work on IoT project

## Week 39 Deliverables

1. Work on IoT project


# Week 40 Topics

Work on IoT project and presentation of work so far.

## Week 40 Tasks

1. Work on IoT project and presentation of work so far.

## Week 40 Deliverables

1. Work on IoT project and presentation of work so far.

# Week 41 Topics

Work on IoT project and presentation of work so far.

## Week 41 Tasks

1. Work on IoT project and presentation of work so far.

## Week 41 Deliverables

1. Present output of commands and document with screen shoots.
